/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BusinessObjects.RealObjects;

import BusinessObjects.AbstractObjects.InstrumentPerkusyjny;
import GUI.Windows.AssignValueWindow;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Mikołaj
 */
public class InstrumentPerkusyjnyElektroniczny extends InstrumentPerkusyjny{
    
    private String rodzajPadow;
    private String wejscia;

    public String getRodzajPadow() {
        return rodzajPadow;
    }

    public void setRodzajPadow(String rodzajPadow) {
        this.rodzajPadow = rodzajPadow;
    }

    public String getWejscia() {
        return wejscia;
    }

    public void setWejscia(String wejscia) {
        this.wejscia = wejscia;
    }
    
    @Override
    public ArrayList<JPanel> generatePropertiesList(){
        ArrayList<JPanel> list = super.generatePropertiesList();
        
        JPanel nazwaPanel = generatePropertyPanel("Rodzaj padów", rodzajPadow);
        nazwaButtonAction(nazwaPanel);
        list.add(nazwaPanel);
                
        JPanel nazwa2Panel = generatePropertyPanel("Wejścia", wejscia);
        nazwa2ButtonAction(nazwa2Panel);
        list.add(nazwa2Panel);
        
        return list;
    }
    
    @Override
    public boolean uzupelnioneWszystkiePozycje(){
        return (!isNullOrEmpty(rodzajPadow) && !isNullOrEmpty(wejscia) && super.uzupelnioneWszystkiePozycje());
    }
    
    private void nazwaButtonAction(JPanel panel){
        ((JButton) panel.getComponent(4)).addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                try {
                    rodzajPadow = AssignValueWindow.ShowWindow(((JLabel) panel.getComponent(2)).getText()).toString();
                    ((JLabel) panel.getComponent(2)).setText(rodzajPadow);
                }catch (Exception ex){
                    //Do nothing just if value isnt correct we leave unchanged
                }
            }
        });    
    }
    
    private void nazwa2ButtonAction(JPanel panel){
        ((JButton) panel.getComponent(4)).addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                try {
                    wejscia = AssignValueWindow.ShowWindow(((JLabel) panel.getComponent(2)).getText()).toString();
                    ((JLabel) panel.getComponent(2)).setText(wejscia);
                }catch (Exception ex){
                    //Do nothing just if value isnt correct we leave unchanged
                }
            }
        });    
    }
}
