/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BusinessObjects.RealObjects;

import BusinessObjects.AbstractObjects.GitaraPodlegajacaUslugom;
import GUI.Windows.AssignValueWindow;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Mikołaj
 */
public class GitaraBasowa extends GitaraPodlegajacaUslugom{
    private int iloscStrun;

    public int getIloscStrun() {
        return iloscStrun;
    }

    public void setIloscStrun(int iloscStrun) {
        this.iloscStrun = iloscStrun;
    }
    
    @Override
    public ArrayList<JPanel> generatePropertiesList(){
        ArrayList<JPanel> list = super.generatePropertiesList();
        
        JPanel nazwaPanel = generatePropertyPanel("Ilość strun", iloscStrun);
        nazwaButtonAction(nazwaPanel);
        list.add(nazwaPanel);
        
        return list;
    }
    
    @Override
    public boolean uzupelnioneWszystkiePozycje(){
        return (super.uzupelnioneWszystkiePozycje() && iloscStrun != 0);
    }
    
    private void nazwaButtonAction(JPanel panel){
        ((JButton) panel.getComponent(4)).addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                try {
                    iloscStrun = Integer.parseInt(AssignValueWindow.ShowWindow(((JLabel) panel.getComponent(2)).getText()));
                    ((JLabel) panel.getComponent(2)).setText(Integer.toString(iloscStrun));
                }catch (Exception ex){
                    //Do nothing just if value isnt correct we leave unchanged
                }
            }
        });    
    }
}
