/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BusinessObjects.RealObjects;

import BusinessObjects.AbstractObjects.Gitara;
import GUI.Windows.AssignValueWindow;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Mikołaj
 */
public class GitaraKlasyczna extends Gitara {
    
    public String getWymiar() {
        return Wymiar;
    }

    public void setWymiar(String Wymiar) {
        this.Wymiar = Wymiar;
    }
    private String Wymiar;
    
    @Override
    public ArrayList<JPanel> generatePropertiesList(){
        ArrayList<JPanel> list = super.generatePropertiesList();
        
        JPanel nazwaPanel = generatePropertyPanel("Wymiar", Wymiar);
        nazwaButtonAction(nazwaPanel);
        list.add(nazwaPanel);
        
        return list;
    }
    
    @Override
    public boolean uzupelnioneWszystkiePozycje(){
        return (!isNullOrEmpty(Wymiar) && super.uzupelnioneWszystkiePozycje());
    }
    
    private void nazwaButtonAction(JPanel panel){
        ((JButton) panel.getComponent(4)).addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                try {
                    Wymiar = AssignValueWindow.ShowWindow(((JLabel) panel.getComponent(2)).getText()).toString();
                    ((JLabel) panel.getComponent(2)).setText(Wymiar);
                }catch (Exception ex){
                    //Do nothing just if value isnt correct we leave unchanged
                }
            }
        });    
    }
}
