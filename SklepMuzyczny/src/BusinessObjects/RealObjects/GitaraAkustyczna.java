/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BusinessObjects.RealObjects;

import BusinessObjects.AbstractObjects.Gitara;
import GUI.Windows.AssignValueWindow;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Mikołaj
 */
public class GitaraAkustyczna extends Gitara{

    public String getRodzajKorpusu() {
        return rodzajKorpusu;
    }

    public void setRodzajKorpusu(String rodzajKorpusu) {
        this.rodzajKorpusu = rodzajKorpusu;
    }

    public String getRodzajDrewna() {
        return rodzajDrewna;
    }

    public void setRodzajDrewna(String rodzajDrewna) {
        this.rodzajDrewna = rodzajDrewna;
    }
    
    private String rodzajKorpusu;
    private String rodzajDrewna;
    
    @Override
    public ArrayList<JPanel> generatePropertiesList(){
        ArrayList<JPanel> list = super.generatePropertiesList();
        
        JPanel rodzajKorpusuPanel = generatePropertyPanel("Rodzaj korpusu", rodzajKorpusu);
        rodzajKorpusuButtonAction(rodzajKorpusuPanel);
        list.add(rodzajKorpusuPanel);
        
        JPanel rodzajDrewnaPanel = generatePropertyPanel("Rodzaj drewna", rodzajDrewna);
        rodzajDrewnaButtonAction(rodzajDrewnaPanel);
        list.add(rodzajDrewnaPanel);
        
        return list;
    }
    
    @Override
    public boolean uzupelnioneWszystkiePozycje(){
        return (super.uzupelnioneWszystkiePozycje() && !isNullOrEmpty(rodzajKorpusu) && !isNullOrEmpty(rodzajDrewna));
    }
    
    private void rodzajKorpusuButtonAction(JPanel panel){
        ((JButton) panel.getComponent(4)).addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                try {
                    rodzajKorpusu = AssignValueWindow.ShowWindow(((JLabel) panel.getComponent(2)).getText()).toString();
                    ((JLabel) panel.getComponent(2)).setText(rodzajKorpusu);
                }catch (Exception ex){
                    //Do nothing just if value isnt correct we leave unchanged
                }
            }
        });    
    }
    
    private void rodzajDrewnaButtonAction(JPanel panel){
        ((JButton) panel.getComponent(4)).addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                try {
                    rodzajDrewna = AssignValueWindow.ShowWindow(((JLabel) panel.getComponent(2)).getText()).toString();
                    ((JLabel) panel.getComponent(2)).setText(rodzajDrewna);
                }catch (Exception ex){
                    //Do nothing just if value isnt correct we leave unchanged
                }
            }
        });    
    }
    
}
