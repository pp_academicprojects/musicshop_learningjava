/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BusinessObjects.AbstractObjects;

import GUI.Windows.AddNewEditProductWindow;
import GUI.Windows.AssignValueWindow;
import com.sun.java.swing.plaf.motif.MotifTreeCellRenderer;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;
import java.util.ArrayList;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Mikołaj
 */
public abstract class Produkt implements Serializable {

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public double getCena() {
        return cena;
    }

    public void setCena(double cena) {
        this.cena = cena;
    }

    public int getNormatyw() {
        return normatyw;
    }

    public void setNormatyw(int normatyw) {
        this.normatyw = normatyw;
    }

    public int getStanNaMagazynie() {
        return stanNaMagazynie;
    }

    public void setStanNaMagazynie(int stanNaMagazynie) {
        this.stanNaMagazynie = stanNaMagazynie;
    }
    private String nazwa;
    private double cena;
    private int normatyw;
    private int stanNaMagazynie;
    
    @Override
    public String toString(){
        return "Produkt";
    }
    
    /**
     * Tworzy formatkę do zmiany/uzupełnienia wartości możliwych do wprowadzenia dla okręslonego produktu
     * @return
     */
    public ArrayList<JPanel> generatePropertiesList(){
        ArrayList<JPanel> list = new ArrayList<>();
        
        JPanel nazwaPanel = generatePropertyPanel("Nazwa", nazwa);
        nazwaButtonAction(nazwaPanel);
        list.add(nazwaPanel);
        
        JPanel cenaPanel = generatePropertyPanel("Cena", cena);
        cenaButtonAction(cenaPanel);
        list.add(cenaPanel);
        
        JPanel normatywPanel = generatePropertyPanel("Normatyw", normatyw);
        normatywButtonAction(normatywPanel);
        list.add(normatywPanel);
        
        JPanel stanNaMagazyniePanel = generatePropertyPanel("Stan na magazynie", stanNaMagazynie);
        stanNaMagazynieButtonAction(stanNaMagazyniePanel);
        list.add(stanNaMagazyniePanel);
        
        return list;
    }
    
    /**
     *Zwraca informację czy na formatce dodawania/edycji produktu są uzupełnione wszystkie wartości niezbędne do zapisania produktu
     * @return
     */
    public boolean uzupelnioneWszystkiePozycje(){
        return (!isNullOrEmpty(nazwa) && cena != 0);
    }
    
    private void nazwaButtonAction(JPanel panel){
        ((JButton) panel.getComponent(4)).addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                try {
                    nazwa = AssignValueWindow.ShowWindow(((JLabel) panel.getComponent(2)).getText()).toString();
                    ((JLabel) panel.getComponent(2)).setText(nazwa);
                }catch (Exception ex){
                    //Do nothing just if value isnt correct we leave unchanged
                }
            }
        });    
    }
    
    private void cenaButtonAction(JPanel panel){
        ((JButton) panel.getComponent(4)).addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                try {
                    cena =  Double.parseDouble(AssignValueWindow.ShowWindow(((JLabel) panel.getComponent(2)).getText()));
                    ((JLabel) panel.getComponent(2)).setText(Double.toString(cena));
                }catch (Exception ex){
                    //Do nothing just if value isnt correct we leave unchanged
                }
            }
        });    
    }
    
    private void normatywButtonAction(JPanel panel){
        ((JButton) panel.getComponent(4)).addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                try {
                    normatyw =Integer.parseInt(AssignValueWindow.ShowWindow(((JLabel) panel.getComponent(2)).getText()));
                    ((JLabel) panel.getComponent(2)).setText(Integer.toString(normatyw));
                }catch (Exception ex){
                    //Do nothing just if value isnt correct we leave unchanged
                }
            }
        });    
    }
    
    private void stanNaMagazynieButtonAction(JPanel panel){
        ((JButton) panel.getComponent(4)).addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                try {
                    stanNaMagazynie = Integer.parseInt(AssignValueWindow.ShowWindow(((JLabel) panel.getComponent(2)).getText()).toString());
                    ((JLabel) panel.getComponent(2)).setText(Integer.toString(stanNaMagazynie));
                }catch (Exception ex){
                    //Do nothing just if value isnt correct we leave unchanged
                }
            }
        });    
    }
            
    protected JPanel generatePropertyPanel(String propertyName, Object propertyValue){
        JPanel pane = new JPanel();
        pane.setLayout(new BoxLayout(pane, BoxLayout.X_AXIS));
        
        JLabel propNameLabel = new JLabel(propertyName);
               
        propertyValue = propertyValue == null ? "" : propertyValue;
        JLabel valueLabel = new JLabel(propertyValue.toString());
        
        JButton changeValueButton = new JButton("Zmień");
        
        pane.add(propNameLabel);
        pane.add(Box.createHorizontalStrut(5));
        pane.add(valueLabel);
        pane.add(Box.createHorizontalStrut(5));
        pane.add(changeValueButton);
        return pane;
    }
    
    /**
     *
     * @param s badany string
     * @return wartość czy badany string jest bez wartości
     */
    public static boolean isNullOrEmpty( final String s ) {
        return s == null || s.trim().isEmpty();
    }
}
