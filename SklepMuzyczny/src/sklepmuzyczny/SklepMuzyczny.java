/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sklepmuzyczny;

import BusinessObjects.AbstractObjects.Produkt;
import BusinessObjects.RealObjects.GitaraKlasyczna;
import GUI.Windows.MainWindow;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;

/**
 *
 * @author Mikołaj
 */
public class SklepMuzyczny {

    /**
     * Rozpoczecia dzialania programu
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException, FileNotFoundException, ClassNotFoundException{
        // TODO code application logic here
        JFrame mainWindow = MainWindow.CreateMainWindow();
        
        mainWindow.addWindowListener(new WindowAdapter(){
                public void windowClosing(WindowEvent e){
                    try {
                        MainWindow.exportData();
                    } catch (IOException ex) {
                        Logger.getLogger(SklepMuzyczny.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });
        
        mainWindow.show();
    }
    
}
