/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.Windows;

import BusinessObjects.AbstractObjects.Produkt;
import GUI.Components.ButtonColumn;
import GUI.Components.ButtonEditor;
import GUI.Components.ButtonRenderer;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author Mikołaj
 */
public class MainWindow{
    
    private static ArrayList<Produkt> filteredMainList;
    private static ArrayList<Produkt> mainList;
    private MainWindow(){}
    private static Container pane;
    private static JFrame frame;
    
    /**
     * Utworzenie i wyświetlenie okna głownego sklepu
     * @return
     * @throws IOException
     * @throws FileNotFoundException
     * @throws ClassNotFoundException
     */
    public static JFrame CreateMainWindow() throws IOException, FileNotFoundException, ClassNotFoundException{
//        MainWindow.mainList = mainList;
        importData();
        frame = new JFrame("Sklep muzyczny");
        BorderLayout bl = new BorderLayout(10,10);
        frame.setLayout(bl);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setExtendedState( frame.getExtendedState()|JFrame.MAXIMIZED_BOTH );
        
        pane = frame.getContentPane();
        
        addComponentsToPane();
        
        refreshView();
        
        frame.pack();
        return frame;
    }  
    
    private static void importData() throws IOException, ClassNotFoundException{
        try{
            ObjectInputStream in = new ObjectInputStream(new FileInputStream("./SerializedData"));
            mainList = (ArrayList<Produkt>)in.readObject();
            in.close();
        }catch(FileNotFoundException ex){
            mainList = new ArrayList<>();
        }
    }
    
    /**
     * Zapisuje dane aktualnego stanu sklepu poprzez serializacje do pliku SerializedData znajdujacego sie w katalogu aplikacji
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static void exportData() throws FileNotFoundException, IOException{
        ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("./SerializedData"));
        out.writeObject(mainList);
        out.close();
    }
    
    private static void addComponentsToPane() {
        addPageStartComponents(); //NotDone
        addCenterComponents(); //NotDone
        addLineEndComponents(); //NotDone
    }
    
    private static JLabel filterLabel;
    private static void addPageStartComponents(){
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
        panel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        
        JLabel label = new JLabel("Filtr: ");
        filterLabel = new JLabel();
        
        JButton zmienWartoscFiltraButton = new JButton("Edytuj");
        zmienWartoscFiltraButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                zmienWartoscFiltra();
            }
        });
        
        JButton usunFiltrowanieButton = new JButton("Usuń");
        usunFiltrowanieButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                usunFiltrowanie();
            }
        });
        
        panel.add(label);
        panel.add(filterLabel);
        panel.add(zmienWartoscFiltraButton);
        panel.add(usunFiltrowanieButton);
        
        pane.add(panel, BorderLayout.PAGE_START);
    }
    
    private static void zmienWartoscFiltra(){
        String ret = AssignValueWindow.ShowWindow(filterLabel.getText());
        if (ret == null || ret.trim().isEmpty())
            return;
        filterLabel.setText(ret);
        refreshTableContent();
    }
    
    private static void usunFiltrowanie(){
        filterLabel.setText("");
        refreshTableContent();
    }
    
    private static Object[][] generateTableContent(){
        generateFilteredList();
        Object ret[][] = new Object[filteredMainList.size()][5];
        for (int i = 0; i < filteredMainList.size(); i++) {
            Produkt prod = filteredMainList.get(i);
            Object rowData[] = { prod.getNazwa(), prod.getCena(), prod.getNormatyw(), prod.getStanNaMagazynie(), "Usuń pozycję" };
            ret[i] = rowData;
        }
        return ret;
    }
    
    private static void generateFilteredList()
    {
        filteredMainList = new ArrayList<>();
        if (filterLabel.getText() == null || filterLabel.getText().trim().isEmpty()){
            filteredMainList = mainList;
            return;
        }
        mainList.stream().filter((prod) -> (prod.getNazwa().contains(filterLabel.getText()))).forEachOrdered((prod) -> {
            filteredMainList.add(prod);
        });
    }
    
    private static JTable table;
    private static JScrollPane scrollPane;
    
    private static void addCenterComponents(){
        
        JPanel listNamePane = new JPanel();
        listNamePane.setLayout(new BoxLayout(listNamePane, BoxLayout.X_AXIS));
        
        JButton newRowButton = new JButton();
        newRowButton.setText("+");
        newRowButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                addNewProdukt();
            }
        });
        
        JLabel infoLabel = new JLabel();
        infoLabel.setText("Dodaj nową pozycję");
        
        listNamePane.add(newRowButton);
        listNamePane.add(infoLabel);
        
        JPanel listPane = new JPanel();
        listPane.setLayout(new BoxLayout(listPane, BoxLayout.Y_AXIS));
        listPane.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 0));
        
        
        
        Object columnNames[] = { "Nazwa", "Cena", "Normatyw", "Stan na magazynie", "Usuń" };
        Object rowData[][] = generateTableContent();
        
        table = new JTable(new DefaultTableModel(rowData, columnNames));
        table.getColumn("Usuń").setCellRenderer(new ButtonRenderer());
        table.getColumn("Usuń").setCellEditor(new ButtonEditor(new JCheckBox()));
        addTableEvents(table);
        
        scrollPane = new JScrollPane(table);
        
        listPane.add(listNamePane);
        listPane.add(scrollPane);
        
        pane.add(listPane, BorderLayout.CENTER);
    }
    
    private static JLabel allProductsCounterLabel;
    private static JLabel gitaryCounterLabel;
    private static JLabel instrumentyKlawiszoweCounterLabel;
    private static JLabel instumentyPerkusyjneCounterLabel;
    private static JLabel sprzetNaglosnieniowyCounterLabel;
    private static JLabel spelniajaceNormatywCounterLabel;
    private static JLabel niespelniajaceNormatywCounterLabel;
    private static void addLineEndComponents(){
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 10));
        
        JPanel allProductsPanel = new JPanel();
        allProductsPanel.setLayout(new BoxLayout(allProductsPanel, BoxLayout.X_AXIS));
        JLabel lab1 = new JLabel("Wszystkich produktów: ");
        allProductsCounterLabel = new JLabel();
        allProductsPanel.add(lab1);
        allProductsPanel.add(allProductsCounterLabel);
        panel.add(allProductsPanel);
        
        JPanel pan2 = new JPanel();
        pan2.setLayout(new BoxLayout(pan2, BoxLayout.X_AXIS));
        JLabel lab2 = new JLabel("Gitar: ");
        gitaryCounterLabel = new JLabel();
        pan2.add(lab2);
        pan2.add(gitaryCounterLabel);
        panel.add(pan2);
        
        JPanel pan3 = new JPanel();
        pan3.setLayout(new BoxLayout(pan3, BoxLayout.X_AXIS));
        JLabel lab3 = new JLabel("Instrumentów klawiszowych: ");
        instrumentyKlawiszoweCounterLabel = new JLabel();
        pan3.add(lab3);
        pan3.add(instrumentyKlawiszoweCounterLabel);
        panel.add(pan3);
        
        JPanel pan4 = new JPanel();
        pan4.setLayout(new BoxLayout(pan4, BoxLayout.X_AXIS));
        JLabel lab4 = new JLabel("Instrumentów perkusyjnych: ");
        instumentyPerkusyjneCounterLabel = new JLabel();
        pan4.add(lab4);
        pan4.add(instumentyPerkusyjneCounterLabel);
        panel.add(pan4);
        
        JPanel pan5 = new JPanel();
        pan5.setLayout(new BoxLayout(pan5, BoxLayout.X_AXIS));
        JLabel lab5 = new JLabel("Sprzętów nagłośnieniowych: ");
        sprzetNaglosnieniowyCounterLabel = new JLabel();
        pan5.add(lab5);
        pan5.add(sprzetNaglosnieniowyCounterLabel);
        panel.add(pan5);
        
        JPanel pan6 = new JPanel();
        pan6.setLayout(new BoxLayout(pan6, BoxLayout.X_AXIS));
        JLabel lab6 = new JLabel("Spełniające normatyw: ");
        spelniajaceNormatywCounterLabel = new JLabel();
        pan6.add(lab6);
        pan6.add(spelniajaceNormatywCounterLabel);
        panel.add(pan6);
        
        JPanel pan7 = new JPanel();
        pan7.setLayout(new BoxLayout(pan7, BoxLayout.X_AXIS));
        JLabel lab7 = new JLabel("Niespełniające normatyw: ");
        niespelniajaceNormatywCounterLabel = new JLabel();
        pan7.add(lab7);
        pan7.add(niespelniajaceNormatywCounterLabel);
        panel.add(pan7);
        
        
        pane.add(panel, BorderLayout.LINE_END);    
    }
    
    private static int tableSelectedRowIndex;
    private static void addTableEvents(JTable table){
        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent me) {
                JTable table =(JTable) me.getSource();
                Point p = me.getPoint();
                tableSelectedRowIndex = table.rowAtPoint(p);
                if (me.getClickCount() == 2 && tableSelectedRowIndex != -1) {
                    tableDoubleClickItem();
                }
            }
        });
    }
    
    /**
     *Usuwa wiersz z listy, który ma focus
     */
    public static void deleteSelectedRowFromMainList(){
        if (tableSelectedRowIndex != -1){
            ArrayList<Produkt> newList = new ArrayList<>();
            int mainListIndex = mainListSelectedIndex();
            int index = 0;
            for (Produkt prod : mainList){
                if (index != mainListIndex)
                    newList.add(prod);
                index ++;
            }
            mainList = newList;
        }
        refreshTableContent();
    }
    
    private static int mainListSelectedIndex()
    {
        Produkt prod = filteredMainList.get(tableSelectedRowIndex);
        int index = 0;
        for (Produkt mprod : mainList){
            if (prod.equals(mprod))
                return index;
            index++;
        }
        return -1;
    }
    
    private static void addNewProdukt(){
        Produkt ret = AddNewEditProductWindow.newProduktWindowShow();
        if (ret != null){
            mainList.add(ret);
            refreshView();
        }
    }
    
    private static void tableDoubleClickItem(){
        Produkt ret = AddNewEditProductWindow.editProduktWindowShow(mainList.get(mainListSelectedIndex()));
        if (ret != null){
            mainList.set(mainListSelectedIndex(), ret);
            refreshView();
        }
    }
    
    private static void refreshView(){
        refreshTableContent();
        refreshGeneralInfo();
    }
    
    private static void refreshTableContent(){
        DefaultTableModel tableModel = (DefaultTableModel) table.getModel();
        tableModel.setRowCount(0);
        for (Object[] data : generateTableContent()){
            tableModel.addRow(data);
        }
        tableModel.fireTableDataChanged();
    }
    
    private static void refreshGeneralInfo(){
        int gitary = 0, instrumentyKlawiszowe = 0, instrumentyPerkusyjne = 0, sprzetyNaglosnieniowe = 0, spelniajaceNormatyw = 0, niespelniajaceNormatyw = 0;
        for (Produkt prod : mainList){
            if ("Gitara".equals(prod.toString()))
                gitary = gitary + prod.getStanNaMagazynie();
            if ("Instrument perkusyjny".equals(prod.toString()))
                instrumentyPerkusyjne = instrumentyPerkusyjne + prod.getStanNaMagazynie();
            if ("Instrument klawiszowy".equals(prod.toString()))
                instrumentyKlawiszowe = instrumentyKlawiszowe + prod.getStanNaMagazynie();
            if ("Sprzet naglosnieniowy".equals(prod.toString()))
                sprzetyNaglosnieniowe = sprzetyNaglosnieniowe + prod.getStanNaMagazynie();
            if (prod.getStanNaMagazynie() >= prod.getNormatyw())
                spelniajaceNormatyw++;
            else
                niespelniajaceNormatyw++;
        }
        allProductsCounterLabel.setText(Integer.toString(gitary + instrumentyKlawiszowe +instrumentyPerkusyjne + sprzetyNaglosnieniowe));
        gitaryCounterLabel.setText(Integer.toString(gitary));
        instrumentyKlawiszoweCounterLabel.setText(Integer.toString(instrumentyKlawiszowe));
        instumentyPerkusyjneCounterLabel.setText(Integer.toString(instrumentyPerkusyjne));
        sprzetNaglosnieniowyCounterLabel.setText(Integer.toString(sprzetyNaglosnieniowe));
        spelniajaceNormatywCounterLabel.setText(Integer.toString(spelniajaceNormatyw));
        niespelniajaceNormatywCounterLabel.setText(Integer.toString(niespelniajaceNormatyw));
    }
}
