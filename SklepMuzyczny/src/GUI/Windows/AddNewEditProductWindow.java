/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.Windows;

import BusinessObjects.AbstractObjects.Produkt;
import BusinessObjects.RealObjects.*;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.ComboBox;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Mikołaj
 */
public class AddNewEditProductWindow {
    
    
    private ArrayList<Class<? extends Produkt>> availableProductTypes;
    
    
    private JDialog frame;
    private Produkt ret;
    private JPanel comboBoxPanel;
    private JPanel produktPropertiesPanel;
    private JPanel bottomPanel;
    
    private AddNewEditProductWindow(){
        this(null);
    }
    
    private AddNewEditProductWindow(Produkt prod){
        ret = prod;
        createJDialog();
        createWindowFramework();
        if (prod == null){
            getAvailableProductTypes();
            addComboBoxComponent();
        }
        addProduktPropertiesComponent();
        addBottomPanelComponents();
        showWindow();
    }
    
    private void showWindow(){
        resizeWindow();
        frame.show();
    }
    
    private void resizeWindow(){
        frame.pack();
        frame.setLocation(getX_Location(), getY_Location());
    }
    
    private int getX_Location(){
        double maxWidth = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
        double freeSpace = maxWidth - frame.getWidth();
        return (int) (freeSpace / 2);
    }
    
    private int getY_Location(){
        double maxHeight = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        double freeSpace = maxHeight - frame.getHeight();
        return (int) (freeSpace / 2);
    }
    
    private void createWindowFramework(){
        JPanel mainStackPanel = new JPanel();
        mainStackPanel.setLayout(new BoxLayout(mainStackPanel, BoxLayout.Y_AXIS));
        
        comboBoxPanel = new JPanel();
        comboBoxPanel.setLayout(new BoxLayout(comboBoxPanel, BoxLayout.X_AXIS));
        
        produktPropertiesPanel = new JPanel();
        produktPropertiesPanel.setLayout(new BoxLayout(produktPropertiesPanel, BoxLayout.Y_AXIS));
        
        bottomPanel = new JPanel();
        bottomPanel.setLayout(new BoxLayout(bottomPanel, BoxLayout.X_AXIS));
        
        mainStackPanel.add(comboBoxPanel);
        mainStackPanel.add(produktPropertiesPanel);
        mainStackPanel.add(bottomPanel);
        
        frame.add(mainStackPanel);
    }
    
    private void createJDialog(){
        frame = new JDialog();
        frame.setModal(true);
    }
    
    private void addComboBoxComponent(){
        JLabel label = new JLabel();
        label.setText("Wybierz grupę produktów");
        
        JComboBox comboBox = new JComboBox();
        
        for (Class availableProductType : availableProductTypes){
            comboBox.addItem(availableProductType.getSimpleName());
        }
        
        comboBox.setSelectedIndex(-1);
        
        comboBox.addActionListener (new ActionListener () {
            public void actionPerformed(ActionEvent e) {
                try {
                    Class<? extends Produkt> produktType = availableProductTypes.get(comboBox.getSelectedIndex());
                    Constructor<?> ctor = produktType.getConstructor();
                    ret = (Produkt)ctor.newInstance();
                    addProduktPropertiesComponent();
                } catch (NoSuchMethodException ex) {
                    Logger.getLogger(AddNewEditProductWindow.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SecurityException ex) {
                    Logger.getLogger(AddNewEditProductWindow.class.getName()).log(Level.SEVERE, null, ex);
                } catch (InstantiationException ex) {
                    Logger.getLogger(AddNewEditProductWindow.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IllegalAccessException ex) {
                    Logger.getLogger(AddNewEditProductWindow.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IllegalArgumentException ex) {
                    Logger.getLogger(AddNewEditProductWindow.class.getName()).log(Level.SEVERE, null, ex);
                } catch (InvocationTargetException ex) {
                    Logger.getLogger(AddNewEditProductWindow.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        
        comboBoxPanel.add(label);
        comboBoxPanel.add(comboBox);
    }
    
    private void getAvailableProductTypes(){
        //TODO pobierać elementy za pomocą reflection xDDDDDDDDDD
        availableProductTypes = new ArrayList<>();
        availableProductTypes.add(GitaraKlasyczna.class);
        availableProductTypes.add(GitaraBasowa.class);
        availableProductTypes.add(GitaraElektryczna.class);
        availableProductTypes.add(GitaraAkustyczna.class);
        availableProductTypes.add(InstrumentKlawiszowyElektroniczny.class);
        availableProductTypes.add(InstrumentKlawiszowyMloteczkowy.class);
        availableProductTypes.add(InstrumentPerkusyjnyElektroniczny.class);
        availableProductTypes.add(InstrumentPerkusyjnyZywy.class);
        availableProductTypes.add(SprzetNaglosnieniowyGlosnik.class);
        availableProductTypes.add(SprzetNaglosnieniowyKonsola.class);
        availableProductTypes.add(SprzetNaglosnieniowyMikrofon.class);
    }
    
    private void addProduktPropertiesComponent(){
        if (ret != null){
            produktPropertiesPanel.removeAll();
            for (JPanel propPanel : ret.generatePropertiesList())
                produktPropertiesPanel.add(propPanel);
            resizeWindow();
        }
    }
    
    private void addBottomPanelComponents(){
        JButton ZapiszButton = new JButton("Zapisz");
        ZapiszButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e ){
                
                if (ret.uzupelnioneWszystkiePozycje())
                    frame.dispose();
            }
        });
        bottomPanel.add(ZapiszButton);
                
        JButton AnulujButton = new JButton("Anuluj");
        AnulujButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e ){
                ret = null;
                frame.dispose();
            }
        });
        bottomPanel.add(AnulujButton);
    }
    
    /**
     *Wyświetla formatkę pozwalającą na dodanie nowego produktu do programu
     * @return
     */
    public static Produkt newProduktWindowShow(){
        AddNewEditProductWindow wind = new AddNewEditProductWindow();
        return wind.ret;
    }
    
    /**
     *Wyświetla formatkę pozwalającą podejrzeć szczegóły produktu oraz umożliwia na dalszą ich modyfikację
     * @return
     */
    public static Produkt editProduktWindowShow(Produkt produktToEdit){
        AddNewEditProductWindow wind = new AddNewEditProductWindow(produktToEdit);
        return wind.ret;
    }
    
}
