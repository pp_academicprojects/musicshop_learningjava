/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.Windows;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Mikołaj
 */
public class AssignValueWindow {
    
    JDialog frame;
    JPanel inputPanel;
    JPanel buttonsPanel;
    
    private AssignValueWindow(Object value){
        createJDialog();
        createWindowFramework();
        addInputPanelControls(value);
        addButtons();
        showWindow();
    }
    private void createJDialog(){
        frame = new JDialog();
        frame.setModal(true);
    }
    
    private void addButtons(){
        JButton ZapiszButton = new JButton("Zapisz");
        ZapiszButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e ){
                ret = ((JTextField) inputPanel.getComponent(1)).getText();
                frame.dispose();
            }
        });
        buttonsPanel.add(ZapiszButton);
                
        JButton AnulujButton = new JButton("Anuluj");
        AnulujButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e ){
                ret = null;
                frame.dispose();
            }
        });
        buttonsPanel.add(AnulujButton);
    }
    
    private void addInputPanelControls(Object value){
        JLabel label = new JLabel("Podaj wartość");
        value = value == null ? "" : value;
        JTextField textField = new JTextField(value.toString());

        inputPanel.add(label);
        inputPanel.add(textField);
    }
    
    private void createWindowFramework(){
        JPanel mainStackPanel = new JPanel();
        mainStackPanel.setLayout(new BoxLayout(mainStackPanel, BoxLayout.Y_AXIS));
        
        inputPanel = new JPanel();
        inputPanel.setLayout(new BoxLayout(inputPanel, BoxLayout.X_AXIS));
        
        buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new BoxLayout(buttonsPanel, BoxLayout.X_AXIS));
        
        mainStackPanel.add(inputPanel);
        mainStackPanel.add(buttonsPanel);
        
        frame.add(mainStackPanel);
    }
    
    private void resizeWindow(){
        frame.pack();
        frame.setLocation(getX_Location(), getY_Location());
    }
    
    private int getX_Location(){
        double maxWidth = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
        double freeSpace = maxWidth - frame.getWidth();
        return (int) (freeSpace / 2);
    }
    
    private int getY_Location(){
        double maxHeight = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        double freeSpace = maxHeight - frame.getHeight();
        return (int) (freeSpace / 2);
    }
    
    private void showWindow(){
        resizeWindow();
        frame.show();
    }
    
    private String ret;
    
    /**
     *Wywołuje formatkę pozwalającą na wpisanie użytkownikowi jakieś wartości i tą wpisaną wartość zwraca.
     * @return
     */
    public static String ShowWindow(){
        return ShowWindow(null);
    }
    
    public static String ShowWindow(Object value){
        AssignValueWindow wind = new AssignValueWindow(value);
        return wind.ret;
    }
    
    
}
